
/*
 *  This file contains an ISA-portable PIN tool for tracing memory accesses.
 */

#include <stdio.h>
#include "pin.H"
#include <fstream>
#include <iostream>
#include <cstring>
#include <math.h> 
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <vector>
#include <sstream>
#include <bitset>
#include <string>
#include <iomanip>
using namespace std;

FILE * trace;
int global_time=1;
/**
  cache block- The basic unit for cache storage. May contain multiple
  bytes/words of data.
  cache line - Same as cache block. Note that this is not the same thing as
  a “row” of cache.
 */
class block{
	public:
		int valid;  
		long long tag;
		int dirty;
		int timestamp; //for lru
		int frequency;  //for lfu

		block(){
			valid = -1;
			tag=-1;
			dirty=0;
			timestamp=global_time;//for lru replacement policy
			frequency=1;
		}
		block(int dirt,int valid_loc,long long tag_loc,int time_stamp,int freq){
			valid = valid_loc;
			tag=tag_loc;
			dirty=dirt;
			timestamp=time_stamp;
			frequency=freq;

		}
};
/**
  cache set - A “row” in the cache. The number of blocks per set is deter-
  mined by the layout of the cache (e.g. direct mapped, set-ass
  ociative,or fully associative)
 */
class cache_set{
	public:
		vector<block> cache_row;
		int num_blocks;

		cache_set(int no_of_cache_rows)
		{
			num_blocks=no_of_cache_rows;
			cache_row.resize(num_blocks);
		}

};

class cache{
	public:
		vector<cache_set> cache_sets;
		int cache_index;
		int no_of_cache_sets; //for set index;
		int block_size; //for block offset
		int hit_count;
		int total_access;
		int miss_ratio;
		int cache_size;
		int associativity;
		int tag_bits_size;
		int set_index_bits_size;
		int block_offset_bits_size;
		int misses;
		string replacement_policy;


		cache(int index,int no_of_sets,int blk_size,int size,int assoc,string policy)
		{
			cache_index=index;
			no_of_cache_sets=no_of_sets;
			block_size=blk_size;
			cache_size=size;
			associativity=assoc;
			block_offset_bits_size=log2(blk_size);
			set_index_bits_size=log2(no_of_sets);
			tag_bits_size =48-tag_bits_size-set_index_bits_size;
			replacement_policy=policy;
			int i;
			hit_count=misses=total_access=0;
			for(i=0;i<no_of_sets;i++)
				cache_sets.push_back(cache_set(assoc));


		}
};

vector <cache> InstructionCache;
vector <cache> DataCache;

/**** for converting void* addresses to binary to be used in calculations *****/
string VoidToBin(VOID * addr)
{
	//cout<<"voidToBin\n";
	stringstream ss1;
	ss1<<((int*)addr);
	long long res = 0;
	//First two characters are '0x' in any hex number
	int i;  
	string ss=ss1.str();
	for(i=2; i<=(int)(ss.size()-1);i++)
	{
		res = (res<<4) + ((ss[i] >= '0' && ss[i] <= '9') ? (ss[i] - '0') : (ss[i] - 'a' + 10));
	}



	string s;
	while(res != 0){
		s.push_back((res&1)+'0');
		res >>= 1;
	}

	while(s.length() < 48)
		s.push_back('0');

	string rev;
	for(i=s.length()-1;i>=0;i--)
		rev.push_back(s[i]);
	return rev;



}
/************ address is of form ---tag(t bits)---set index(s bits)-----block offset( b bits) ***/
/**** If the block size is B then b = log2(B) bits  *********************/
/**** If S is the number of sets in our cache, then the set index has s = log2 (S) bits. ****/ 
/***** t=l-b-s ****/


long long power(int x, unsigned int a)
{

	long long ret = 1;
	long long ini = 2;
	while(a){
		if(a&1)
			ret *= ini;
		a >>= 1;
		ini *= ini;
	}
	return ret;
}


long long get_tag_bits(cache& obj,VOID* addr)
{
	//cout<<"getTag\n";
	string str=VoidToBin(addr);
	int i;
	long long result=0;
	for(i=0;i<obj.tag_bits_size;i++)
	{
		result=(result<<1) +(str[i]-'0');
	}
	return result;


}

long long get_setindex_bits(cache& obj,VOID* addr)
{
	//cout<<"getSetIndex\n";

	string str=VoidToBin(addr);
	//cout<<"aftervoidbin in getset\n";
	int i;
	long long result=0;
	for(i=obj.tag_bits_size;i <= (obj.tag_bits_size+obj.set_index_bits_size-1) ;i++)
	{
		result=(result<<1) +(str[i]-'0');
	}
	return result;


}

long long get_block_offset_bits(cache& obj,VOID* addr)
{
	//cout<<"getBlockOffset\n";
	string str=VoidToBin(addr);
	int i;
	long long result=0;
	for(i=(obj.tag_bits_size+obj.set_index_bits_size);i <= (obj.tag_bits_size+obj.set_index_bits_size+obj.block_offset_bits_size-1) ;i++)
	{
		result=(result<<1) +(str[i]-'0');
	}
	//cout<<"EndgetBlockOffset\n";
	return result;


}
long long CreateAddress(cache& obj,long long tag_bits,long long index,long long block_offset)
{
	//cout<<"createAddress\n";
	//assert(tag_bits!=-1);
	//assert(index!=-1);
	long long result=tag_bits*pow(2,obj.set_index_bits_size+obj.block_offset_bits_size)+index*pow(2,obj.block_offset_bits_size)+block_offset;

	return result;
}

bool IsBlockInCache(cache& obj,VOID *addr)
{
	//cout<<"Isblock in cache\n";
	long long set_index_bits=get_setindex_bits(obj,addr);
	long long tag_bits=get_tag_bits(obj,addr);
	string replacement_policy=obj.replacement_policy;
	obj.total_access++;
	// cout<<"is block begin "<<addr<<"\n";
	for(vector<block>::iterator it=obj.cache_sets[set_index_bits].cache_row.begin();it!=obj.cache_sets[set_index_bits].cache_row.end();++it)
	{
		if(it->tag==tag_bits)
		{
			//cout<<"hit cache yayee at index"<<obj.cache_index<<"\n";
			obj.hit_count++;
			if(replacement_policy.compare("lru"))
				it->timestamp=global_time;
			else if(replacement_policy.compare("lfu"))
				it->frequency++;
			else{}
			return true;
		}
	}
	obj.misses++;
	//cout<<"miss :( at level" << obj.cache_index;
	return false;


}




void WriteBack(cache& obj,VOID *addr)

{
	//cout<<"writeBack\n";
	long long set_index_bits=get_setindex_bits(obj,addr);
	long long tag_bits=get_tag_bits(obj,addr);
	string replacement_policy=obj.replacement_policy;


	for(vector<block>::iterator it=obj.cache_sets[set_index_bits].cache_row.begin();it!=obj.cache_sets[set_index_bits].cache_row.end();++it)
	{

		if(it->tag==tag_bits && replacement_policy=="lru")
		{
			it->timestamp=global_time;
			it->dirty=1;
			return ;
		}
		else if(it->tag==tag_bits && replacement_policy=="lfu")
		{
			it->frequency++;
			it->dirty=1;
			return ;
		}
		else if(it->tag==tag_bits && replacement_policy=="rr")
		{
			it->dirty=1;
			return ;
		}

		else continue;


	}
	return ;
}


// evict a block on lru,lfu or rr
void EvictBlock(cache& obj,long long addr,long long tag,long long set_index)
{
	//cout<<"Evict\n";
	long long set_index_bits=set_index;
	long long tag_bits=tag;
	string replacement_policy=obj.replacement_policy;


	for(vector<block>::iterator it=obj.cache_sets[set_index_bits].cache_row.begin();it!=obj.cache_sets[set_index_bits].cache_row.end();++it)
	{

		if(replacement_policy.compare("lru") && it->tag == tag_bits)
		{
			it->tag=-1;
			it->timestamp=1;
			break;
		}
		else if(replacement_policy.compare("lfu") && it->tag == tag_bits)
		{
			it->tag=-1;
			it->frequency=1;
			break;
		}
		else if(replacement_policy.compare("rr") && it->tag == tag_bits)
		{
			it->tag=-1;
			break;
		}
		else{}

	}
}

void AddBlockToCache(cache& obj,VOID *addr,bool data)
{
	//cout<<"addBlock\n";
	long long set_index_bits=get_setindex_bits(obj,addr);
	long long tag_bits=get_tag_bits(obj,addr);
	string replacement_policy=obj.replacement_policy;
	long long tag_bits_to_replace=-1;
	int block_no_to_replace=-1;

	//  cout<<"add block begin "<< addr <<" \n";
	if(replacement_policy.compare("lru") )
	{
	//	cout<<"add block lru "<< " \n";
		int block_no=0;
		int least_timestamp=obj.cache_sets[set_index_bits].cache_row.begin()->timestamp;
		for(vector<block>::iterator it=obj.cache_sets[set_index_bits].cache_row.begin();it!=obj.cache_sets[set_index_bits].cache_row.end();++it,++block_no)
		{
			if(least_timestamp>it->timestamp)
			{
				least_timestamp=it->timestamp;
				tag_bits_to_replace=it->tag;
				block_no_to_replace=block_no;

			}



		}
		//assert(block_no_to_replace != -1 && least_timestamp > 0);
		//block no -have to do something for that
		/////////////////////////////////////

		if(block_no_to_replace!=-1)
		{


			int i;

			long long remove_address=CreateAddress(obj,tag_bits_to_replace,set_index_bits,block_no_to_replace);
			for(i=obj.cache_index;i>=0;i--)
			{
				if(data){

					long long set_index=get_setindex_bits(DataCache[i],addr);
					long long tag=get_tag_bits(DataCache[i],addr);

					EvictBlock(DataCache[i],remove_address,tag,set_index);
				}				
				else{
					long long set_index=get_setindex_bits(InstructionCache[i],addr);
					long long tag=get_tag_bits(InstructionCache[i],addr);
					EvictBlock(InstructionCache[i],remove_address,tag,set_index);
				}
			}


		}
		// cout<<"add block end\n";
		//int i;
		obj.cache_sets[set_index_bits].cache_row[block_no_to_replace]=block(0,1,tag_bits,global_time,1);
		for(int j=0;j<obj.cache_index;j++)
		{
			if(data && IsBlockInCache(DataCache[j],addr))
			{  long long set_index=get_setindex_bits(DataCache[j],addr);
				long long tag=get_tag_bits(DataCache[j],addr);




				for(vector<block>::iterator it=DataCache[j].cache_sets[set_index].cache_row.begin();it!=DataCache[j].cache_sets[set_index].cache_row.end();++it)
				{

					if(it->tag==tag )
					{
						it->valid=0;
						if(it->dirty==1) WriteBack(DataCache[j+1],addr);
					}


				}


			}
			else if (!data && IsBlockInCache(InstructionCache[j],addr))
			{ long long set_index=get_setindex_bits(InstructionCache[j],addr);
				long long tag=get_tag_bits(InstructionCache[j],addr);

				for(vector<block>::iterator it=InstructionCache[j].cache_sets[set_index].cache_row.begin();it!=InstructionCache[j].cache_sets[set_index].cache_row.end();++it)
				{

					if(it->tag==tag )
					{
						it->valid=0;
						if(it->dirty==1) WriteBack(InstructionCache[j+1],addr);
					}


				}

			}
		}
	}
	else if(replacement_policy.compare("lfu") )
	{

		int block_no=0;
		int least_freq=obj.cache_sets[set_index_bits].cache_row.begin()->frequency;
		for(vector<block>::iterator it=obj.cache_sets[set_index_bits].cache_row.begin();it!=obj.cache_sets[set_index_bits].cache_row.end();++it,++block_no)
		{
			if(least_freq>it->frequency)
			{
				least_freq=it->frequency;
				tag_bits_to_replace=it->tag;
				block_no_to_replace=block_no;

			}



		}

		//obj.cache_sets[set_index_bits].cache_row[block_no]=block(1,tag_bits,global_time,1);
		if(block_no_to_replace!=-1)
		{


			int i;

			long long remove_address=CreateAddress(obj,tag_bits_to_replace,set_index_bits,block_no_to_replace);
			for(i=obj.cache_index;i>=0;i--)
			{
				if(data){

					long long set_index=get_setindex_bits(DataCache[i],addr);
					long long tag=get_tag_bits(DataCache[i],addr);

					EvictBlock(DataCache[i],remove_address,tag,set_index);
				}				
				else{
					long long set_index=get_setindex_bits(InstructionCache[i],addr);
					long long tag=get_tag_bits(InstructionCache[i],addr);
					EvictBlock(InstructionCache[i],remove_address,tag,set_index);
				}			
			}


		}

		obj.cache_sets[set_index_bits].cache_row[block_no_to_replace]=block(0,1,tag_bits,global_time,1);

		for(int j=0;j<obj.cache_index;j++)
		{
			if(data && IsBlockInCache(DataCache[j],addr))
			{  long long set_index=get_setindex_bits(DataCache[j],addr);
				long long tag=get_tag_bits(DataCache[j],addr);




				for(vector<block>::iterator it=DataCache[j].cache_sets[set_index].cache_row.begin();it!=DataCache[j].cache_sets[set_index].cache_row.end();++it)
				{

					if(it->tag==tag )
					{
						it->valid=0;
						if(it->dirty==1) WriteBack(DataCache[j+1],addr);
					}


				}


			}
			else if (!data && IsBlockInCache(InstructionCache[j],addr))
			{ long long set_index=get_setindex_bits(InstructionCache[j],addr);
				long long tag=get_tag_bits(InstructionCache[j],addr);

				for(vector<block>::iterator it=InstructionCache[j].cache_sets[set_index].cache_row.begin();it!=InstructionCache[j].cache_sets[set_index].cache_row.end();++it)
				{

					if(it->tag==tag )
					{
						it->valid=0;
						if(it->dirty==1) WriteBack(InstructionCache[j+1],addr);
					}


				}

			}
		}


	}
	else 
	{


		block_no_to_replace= rand() % (obj.associativity) ;


		//obj.cache_sets[set_index_bits].cache_row[block_no]=block(1,tag_bits,global_time,1);
		if(block_no_to_replace!=-1)
		{


			int i;

			long long remove_address=CreateAddress(obj,tag_bits_to_replace,set_index_bits,block_no_to_replace);
			for(i=obj.cache_index;i>=0;i--)
			{
				if(data){

					long long set_index=get_setindex_bits(DataCache[i],addr);
					long long tag=get_tag_bits(DataCache[i],addr);

					EvictBlock(DataCache[i],remove_address,tag,set_index);
				}				
				else{
					long long set_index=get_setindex_bits(InstructionCache[i],addr);
					long long tag=get_tag_bits(InstructionCache[i],addr);
					EvictBlock(InstructionCache[i],remove_address,tag,set_index);
				}
			}


		}

		obj.cache_sets[set_index_bits].cache_row[block_no_to_replace]=block(0,1,tag_bits,global_time,1);
	}


}



void display()
{
	cout<<"dcache info \n";
	int i;
	ofstream myfile;
	myfile.open ("output.txt");
	myfile << "Writing this to a file.\n";

	for(i=0;i<(int)DataCache.size();i++)
	{
		myfile<<"=====================\n";
		myfile<<"Dcache level "<<(i+1)<<"\n";
		myfile<<"hit count "<<DataCache[i].hit_count<<"\n";
		//myfile<<"misses "<<DataCache[i].misses<<"\n";
		myfile<<"total access count "<<DataCache[i].total_access<<"\n";
		myfile<<"miss ratio "<<(DataCache[i].misses*1.0/(DataCache[i].hit_count+DataCache[i].misses))<<"\n";;


	}
	cout<<"icache info \n";
	for(i=0;i<(int)InstructionCache.size();i++)
	{
		myfile<<"========================\n";
		myfile<<"Icache level "<<(i+1)<<"\n";
		myfile<<"hit count "<<InstructionCache[i].hit_count<<"\n";
		//myfile<<"misses"<<InstructionCache[i].misses<<"\n";
		myfile<<"total access count "<<InstructionCache[i].total_access<<"\n";
		myfile<<"miss ratio "<<(InstructionCache[i].misses*1.0/(InstructionCache[i].hit_count+InstructionCache[i].misses))<<"\n";

	}

	myfile.close();
}

/*
 ** READ **
 1. Use the set index to determine which cache set the address should reside in.
 2. For each block in the corresponding cache set, compare the tag associated with that block to the tag from the memory address. 
 If there is a match, proceed to the next step. Otherwise, the data is not in the cache.
 3. For the block where the data was found, look at valid bit. If it is 1, the data is in the cache, otherwise it is not.

 */
void simulateRead(VOID * addr, bool isDataCache)
{

	//cout<<"simulateRead\n";
	if(isDataCache) 
	{
		int i;
		for(i = 0; i < (int)DataCache.size(); ++i) 
		{
			//cout<<(i+1)<<"\n";
			if(IsBlockInCache(DataCache[i], addr)) 
				break;
		}

		for(int j = i-1; j >= 0; --j)
			AddBlockToCache(DataCache[j], addr,true);

	}
	else
	{
		int i;
		for(i = 0; i < (int)InstructionCache.size(); ++i) 
		{

			if(IsBlockInCache(InstructionCache[i], addr)) 
				break;
		}

		for(int j = i-1; j >= 0; --j)
			AddBlockToCache(InstructionCache[j], addr,false);
	}

	global_time++;
}

// Print a memory read record
VOID RecordMemRead(VOID * ip, VOID * addr)
{
	fprintf(trace,"%p: R %p\n", ip, addr);
	simulateRead(ip, false);
	simulateRead(addr,true);
}

// Print a memory write record


void simulateWrite(VOID * addr, bool isDataCache)
{

	//cout<<"simulateWrite\n";
	if(isDataCache) 
	{
		int i,j;
		long long set_index_bits=0;
		long long block_offset=0;
		for(i = 0; i < (int)DataCache.size(); ++i) 
		{
			//	cout<<(i+1)<<"\n";
			set_index_bits=get_setindex_bits(DataCache[i],addr);

			block_offset =get_block_offset_bits(DataCache[i],addr);
			if(IsBlockInCache(DataCache[i], addr)) 
			{
				//long long set_index_bits=get_setindex_bits(DataCache[i],addr);

				//long long block_offset =get_block_offset_bits(DataCache[i],addr);
				//cout<<"hey";
	//			cout<<"i"<<i<<"Setindex"<<set_index_bits<<"blc offset"<<block_offset<<"\n";
				/**	if(block_offset>0)
				  {
				  if(i+1<(int)DataCache.size()  && DataCache[i].cache_sets[set_index_bits].cache_row[block_offset].dirty==1)
				  {
				  DataCache[i+1].total_access++;
				  DataCache[i+1].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;
				  }		
				  DataCache[i].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;
				//string replacement_policy=obj.replacement_policy;
				}*/
				break;

			}
		}
		for(j=0;j<i;j++)
		{
			AddBlockToCache(DataCache[j],addr,true);
			DataCache[j].total_access++;
		}
		if(block_offset>0)
			DataCache[0].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;
		//assert(i+1< (int)DataCache.size());


		//		WriteBack(DataCache[i+1], addr);


	}
	else
	{
		int i,j;
		long long set_index_bits=0;
		long long block_offset=0;
		for(i = 0; i < (int)InstructionCache.size(); ++i) 
		{
			set_index_bits=get_setindex_bits(InstructionCache[i],addr);
			//long long tag_bits=get_tag_bits(DataCache[i],addr);
			block_offset =get_block_offset_bits(InstructionCache[i],addr);

			if(IsBlockInCache(InstructionCache[i], addr)) 
			{

				//cout<<"hey";
	//			cout<<"i"<<i<<"Setindex"<<set_index_bits<<"blc offset"<<block_offset<<"\n";
				/**	if(block_offset>0)
				  {
				  if(i+1<(int)InstructionCache.size()  && (InstructionCache[i].cache_sets[set_index_bits].cache_row[block_offset].dirty)==1)
				  InstructionCache[i+1].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;
				  InstructionCache[i].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;
				//string replacement_policy=obj.replacement_policy;
				}*/
				break;


			}
		}
		for(j=0;j<i;j++)
		{
			AddBlockToCache(InstructionCache[j],addr,true);
			InstructionCache[j].total_access++;
		}
		if(block_offset>0)
			InstructionCache[0].cache_sets[set_index_bits].cache_row[block_offset].dirty=1;

		//WriteBack(InstructionCache[i+1], addr);
	}

	global_time++;
}
VOID RecordMemWrite(VOID * ip, VOID * addr)
{
	fprintf(trace,"%p: W %p\n", ip, addr);
	simulateWrite(ip,false);
	simulateWrite(addr,true);
}



// Is called for every instruction and instruments reads and writes
VOID Instruction(INS ins, VOID *v)
{
	// Instruments memory accesses using a predicated call, i.e.
	// the instrumentation is called iff the instruction will actually be executed.
	//
	// On the IA-32 and Intel(R) 64 architectures conditional moves and REP 
	// prefixed instructions appear as predicated instructions in Pin.
	UINT32 memOperands = INS_MemoryOperandCount(ins);

	// Iterate over each memory operand of the instruction.
	for (UINT32 memOp = 0; memOp < memOperands; memOp++)
	{
		if (INS_MemoryOperandIsRead(ins, memOp))
		{
			INS_InsertPredicatedCall(
					ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,
					IARG_INST_PTR,
					IARG_MEMORYOP_EA, memOp,
					IARG_END);
		}
		// Note that in some architectures a single memory operand can be 
		// both read and written (for instance incl (%eax) on IA-32)
		// In that case we instrument it once for read and once for write.
		if (INS_MemoryOperandIsWritten(ins, memOp))
		{
			INS_InsertPredicatedCall(
					ins, IPOINT_BEFORE, (AFUNPTR)RecordMemWrite,
					IARG_INST_PTR,
					IARG_MEMORYOP_EA, memOp,
					IARG_END);
		}
	}
}

VOID Fini(INT32 code, VOID *v)
{
	fprintf(trace, "#eof\n");
	fclose(trace);
	display();
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
	PIN_ERROR( "This Pintool prints a trace of memory addresses\n" 
			+ KNOB_BASE::StringKnobSummary() + "\n");
	return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */


void readInput()
{

	int levels,size,block_size,associativity,latency;
	string policy;
	int no_of_cache_sets;  
	//cout<<"hello";
	//cout<<"enter no of levels";
	cin>>levels;

	int i=0;
	while(i<levels)

	{
		cout<<"\n===============";
		cout<<"\nlevel is "<<(i+1);
		cout<<"\nenter size ";
		cin>>size;
		cout<<"\nassociativity ";
		cin>>associativity;
		cout<<"\nenter block size ";
		cin>>block_size; 
		cout<<"\nenter hit latency ";
		cin>>latency;
		cout<<"\nenter replacement policy ";
		cin>>policy;
		no_of_cache_sets= size*1024/(block_size*associativity);


		DataCache.push_back(cache(i,no_of_cache_sets,block_size,size,associativity,policy));
		i++;
	}
	i=0;
	while(i<levels)

	{
		cout<<"\n===============";
		cout<<"\nlevel is "<<(i+1);
		cout<<"\nenter size ";
		cin>>size;
		cout<<"\nassociativity ";
		cin>>associativity;
		cout<<"\nenter block size ";
		cin>>block_size; 
		cout<<"\nenter hit latency ";
		cin>>latency;
		cout<<"\nenter replacement policy ";
		cin>>policy;
		no_of_cache_sets= size*1024/(block_size*associativity);


		InstructionCache.push_back(cache(i,no_of_cache_sets,block_size,size,associativity,policy));
		i++;
	}


}
int main(int argc, char *argv[])
{
	if (PIN_Init(argc, argv)) return Usage();

	trace = fopen("pinatrace.out", "w");
	readInput();
	INS_AddInstrumentFunction(Instruction, 0);
	PIN_AddFiniFunction(Fini, 0);

	// Never returns
	PIN_StartProgram();

	return 0;
}
